import java.math.*;
import java.util.*;
import java.util.stream.*;

class GSO
{
    private BigDecimal[][] gsom; // Gram-Schmidt-Orthogonalbasis als Matrix
    private BigDecimal[][] M; // Matrix M, welche müs festhält
    public GSO(BigDecimal[][] gsom, BigDecimal[][] M)
    {
        this.gsom = gsom;
        this.M = M;

    }
    public BigDecimal[][] getGsom() { return gsom; }
    public BigDecimal[][] getM() { return M; }
}

class Instance
{
    private BigInteger[] a; // Folge a einer Subset Sum Problem Instanz (a,s)
    private BigInteger s; // Teilsumme s für Subset Sum Problem Instanz (a,s)
    private int[] e; // Lösung für Subset Sum Problem Instanz (a,s)
    private double d; // Dichte von a
    public Instance(BigInteger[] a, BigInteger s, int[] e, double d)
    {
        this.a = a;
        this.s = s;
        this.e = e;
        this.d = d;

    }
    public BigInteger[] getA() { return a; }
    public BigInteger getS() { return s; }
    public int[] getE() { return e; }
    public double getD() { return d; }
    public void updateA(BigInteger[] a) { this.a = a; }
    public void updateE(int[] e) { this.e = e; }
}

public class SV {
  public static void main(String[] args){
    // Eingaben von Kommandozeile entgegennehmen
    Scanner reader = new Scanner(System.in);
    System.out.print("Gebe eine Subset Sum Problem Instanzgröße n an: ");
    int instance_size = reader.nextInt();
    while (instance_size < 2) {
      System.out.print("Gebe eine Subset Sum Problem Instanzgröße n an: ");
      instance_size = reader.nextInt();
    }
    System.out.println("");
    System.out.print("Gebe H für obere Schranke X=2^H von a an: ");
    int upper_bound_exponent = reader.nextInt();
    while (upper_bound_exponent < 1) {
      System.out.print("Gebe H für obere Schranke X=2^H von a an: ");
      upper_bound_exponent = reader.nextInt();
    }
    System.out.println("");
    System.out.print("Gebe '5' (5 Versuche) oder '1' (1 Versuch) ein: ");
    int five = reader.nextInt();
    while (five != 1 && five != 5) {
      System.out.print("Gebe '5' (5 Versuche) oder '1' (1 Versuch) ein: ");
      five = reader.nextInt();
    }
    System.out.println("");
    reader.close();

    int genauigkeit = 60; // Parameter Genauigkeit hier setzen

    // erster Versuch des SV-Algorithmus auf a
    long startTime = System.nanoTime();
    Instance instance = createInstance(instance_size, upper_bound_exponent);
    boolean won = calculate_sv(instance, genauigkeit, true);
    long stopTime = System.nanoTime();
    System.out.println("");
    System.out.println("Laufzeit (Sekunden): " + (double)(stopTime - startTime) / (double)1000000000);
    System.out.println("");
    
    // vier weitere Versuche des SV-Algorithmus auf Permutationen von a,
    // vorausgesetzt der erste war nicht erfolgreich
    if (!won && five == 5) {
      System.out.println("Vier weitere Versuche mit Permutationen von a:");
      System.out.println("");
      for (int i=0; i<4; i++) {
        if (won) {
          break;
        }
        // Permutation von a und e
        BigInteger[] a = instance.getA();
        int[] e = instance.getE();
        int len_a = a.length;
        List<Integer> list = new ArrayList<Integer>();
        for (int j=0; j < len_a; j++) {
          list.add(j);
        }
        java.util.Collections.shuffle(list);
        BigInteger[] new_a = new BigInteger[len_a];
        int[] new_e = new int[len_a];
        for (int j=0; j < len_a; j++) {
          new_a[j] = a[list.get(j)];
          new_e[j] = e[list.get(j)];
        }
        instance.updateA(new_a);
        instance.updateE(new_e);
        // SV-Algorithmus auf Permutation von a
        won = calculate_sv(instance, genauigkeit, false);
      }
    }
  }

  public static boolean calculate_sv(Instance instance, int accuracy, boolean print) {
    // SV-Algorithmus nach Pseudocode
    BigInteger[] a = instance.getA();
    BigInteger s = instance.getS();
    int[] e = instance.getE();
    
    if (print) {
      print_instance(instance);
    }

    BigDecimal[][] matrix = createMatrix(a,s);
    //print_matrix(matrix, "matrix");
    BigDecimal[][] short_matrix = lll(matrix, accuracy);
    //print_matrix(short_matrix, "short_matrix");

    boolean result = check_result(short_matrix, e);
    
    if (result) {
      System.out.println("erster Durchlauf: Erfolg");
    }else {
      System.out.println("erster Durchlauf: Misserfolg");
    }

    if (!result) {
      BigInteger sum = new BigInteger("0");
      
      for (int i = 0; i < a.length; i++) {
        sum = sum.add(a[i]);
      }
      
      BigInteger new_s = sum.subtract(s);
      matrix = createMatrix(a,new_s);
      short_matrix = lll(matrix, accuracy);
      int[] new_e = e;
      
      for (int i = 0; i < e.length; i++) {
        new_e[i] = 1 - e[i];
      }
      
      //print_matrix(short_matrix, "short_matrix");
      result = check_result(short_matrix, new_e);
      
      if (result) {
        System.out.println("zweiter Durchlauf: Erfolg");
      }else {
        System.out.println("zweiter Durchlauf: Misserfolg");
      }
      
      if (!print) {
        System.out.println("------");
      }
    }
    
    return result;
  }

  public static void print_matrix(BigDecimal[][] matrix, String name) {
    // printe Matrix, wurde für Testzwecke verwendet
    int len = matrix[0].length;
    System.out.println("### " + name + " ###");
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println("");
    }
  }

  public static void print_instance(Instance instance) {
    // printe Subset Sum Problem Instanz Informationen
    BigInteger[] a = instance.getA();
    BigInteger s = instance.getS();
    int[] e = instance.getE();
    double d = instance.getD();

    int len = a.length;
    
    System.out.println("--- Dichte ---");
    System.out.println(d);
    System.out.println("");

    System.out.println("--- e ---");
    for (int i = 0; i < len; i++) {
      System.out.print(e[i] + " ");
    }
    System.out.println("");
    System.out.println("");

    System.out.println("--- a ---");
    for (int i = 0; i < len; i++) {
        System.out.println(a[i]);
      }
    System.out.println("");
    
    System.out.println("--- s ---");
    System.out.println(s);
    System.out.println("");
  }

  public static Instance createInstance(int n, int H) {
    // erstelle Subset Sum Problem Instanz aus Länge n für a
    // und Exponent H für obere Schranke X=2^H der Elemente in a
    BigInteger[] a = new BigInteger[n];
    BigInteger s = new BigInteger("0");
    int[] e = new int[n];
    BigInteger X = new BigInteger("2");
    int exponent = H;
    X = X.pow(exponent); // obere Schranke für Elemente in a
    Random random = new Random();
    
    // zufällige BigInteger für Folge a
    for (int i = 0; i < n; i++) {
      BigInteger ran_big_int = new BigInteger(X.bitLength(), random);
      
      while(ran_big_int.compareTo(X) >= 0 || ran_big_int.compareTo(new BigInteger("0")) == 0) {
        ran_big_int = new BigInteger(X.bitLength(), random);
      }

      a[i] = ran_big_int;
    }

    // zufällige Wahl von e
    for (int i=0; i< n; i++) {
      e[i] = random.nextInt(2);
    }

    // Bedingung sum(e) <= n/2 herstellen
    while (IntStream.of(e).sum() > n/2 || IntStream.of(e).sum() == 0) {
      e = new int[n];
      
      for (int i=0; i< n; i++) {
        e[i] = random.nextInt(2);
      }

    }

    // Berechnung von s
    for (int i=0; i< n; i++) {
      if (e[i]==1) {
        s = s.add(a[i]);
      }
    }

    // Dichte berechnen aus H und n
    double d = (double)n / (double)exponent;

    return new Instance(a,s,e,d);
  }

  public static BigDecimal[][] createMatrix(BigInteger[] a, BigInteger s) {
    // Subset Sum Problem Instanz als Basis eines Gitters
    // in Matrixform
    int len = a.length + 1;
    BigDecimal[][] matrix = new BigDecimal[len][len];
    
    for (int i = 0; i < len - 1; i++) {
      Arrays.fill(matrix[i], BigDecimal.ZERO);
      matrix[i][i] = new BigDecimal("1");
      BigDecimal minus_eins = new BigDecimal("-1");
      matrix[i][len-1] = minus_eins.multiply(new BigDecimal(a[i]));
    }

    Arrays.fill(matrix[len-1], BigDecimal.ZERO);
    matrix[len-1][len-1] = new BigDecimal(s);

    return matrix;
  }

  public static boolean check_result(BigDecimal[][] short_matrix, int[] e) {
    // prüfen ob e oder -e in Lösung vom SV-Algorithmus enthalten ist
    int len = short_matrix[0].length;
    
    for (int i = 0; i < len; i++) {
      boolean match = true; // e enthalten
      boolean match_m = true; // -e enthalten
      
      for (int j = 0; j < len-1; j++) {
        BigDecimal compare_to = new BigDecimal(e[j]);
        
        if (short_matrix[i][j].compareTo(compare_to) != 0) {
          match = false;
        }
        
        if (short_matrix[i][j].compareTo(compare_to.multiply(new BigDecimal("-1"))) != 0) {
          match_m = false;
        }
      }

      if (match || match_m) {
        return true;
      }else {
        match = true;
        match_m = true;
      }
    }

    return false;
  }

  public static BigDecimal skalarprodukt(BigDecimal[] vek_1, BigDecimal[] vek_2) {
    // Skalarprodukt von BigDecimal Vektoren
    int len = vek_1.length;
    BigDecimal skalarprod = new BigDecimal(0);
    
    for (int i = 0; i < len; i++) {
      skalarprod = skalarprod.add(vek_1[i].multiply(vek_2[i]));
    }

    return skalarprod;
  }

  public static BigDecimal[] scale(BigDecimal[] vek, BigDecimal sklr) {
    // skalieren eines BigDecimal Vektors
    int len = vek.length;
    BigDecimal[] skaliert = new BigDecimal[len];
    
    for (int i = 0; i < len; i++) {
      skaliert[i] = vek[i].multiply(sklr);
    }

    return skaliert;
  }

  public static BigDecimal[] vek_subtract(BigDecimal[] vek_1, BigDecimal[] vek_2) {
    // zwei BigDecimal Vektoren subtrahieren
    int len = vek_1.length;
    BigDecimal[] vektor = new BigDecimal[len];
    
    for (int i = 0; i < len; i++) {
      vektor[i] = vek_1[i].subtract(vek_2[i]);
    }

    return vektor;
  }

  public static BigDecimal[] vek_add(BigDecimal[] vek_1, BigDecimal[] vek_2) {
    // zwei BigDecimal Vektoren addieren
    int len = vek_1.length;
    BigDecimal[] vektor = new BigDecimal[len];
    
    for (int i = 0; i < len; i++) {
      vektor[i] = vek_1[i].add(vek_2[i]);
    }

    return vektor;
  }

  public static BigDecimal[][] swap_vektors(BigDecimal[][] matrix, int pos_1, int pos_2) {
    // vertausche zwei BigDecimal Vektoren in einer BigDecimal Matrix
    int len = matrix[0].length;
    BigDecimal[][] new_matrix = new BigDecimal[len][len];
    
    for (int i = 0; i < len; i++) {
      new_matrix[pos_1][i] = matrix[pos_2][i];
      new_matrix[pos_2][i] = matrix[pos_1][i];
      
      if (i != pos_1 && i != pos_2) {
        new_matrix[i] = matrix[i];
      }
    }

    return new_matrix;
  }
  
  public static GSO gso(BigDecimal[][] matrix, int accuracy){
    // berechne Gram-Schmidt-Orthogonalmatrix
    int len = matrix[0].length;
    BigDecimal[][] gsom = new BigDecimal[len][len];
    BigDecimal[][] M = new BigDecimal[len][len];

    for (int i = 0; i < len; i++) {
      M[i][i] = new BigDecimal("1");
    }

    gsom[0] = matrix[0];

    for (int i = 1; i < len; i++) {
      BigDecimal[] minus = new BigDecimal[len];
      Arrays.fill(minus, BigDecimal.ZERO);
      
      for (int j = 0; j < i; j++) {
        BigDecimal m = skalarprodukt(matrix[i], gsom[j]).divide(skalarprodukt(gsom[j], gsom[j]), accuracy, RoundingMode.HALF_UP);
        M[j][i] = m;
        minus = vek_add(minus, scale(gsom[j], m));
      }

      gsom[i] = vek_subtract(matrix[i], minus);
    }

    return new GSO(gsom, M);
  }

  public static BigDecimal[][] lll(BigDecimal[][] matrix, int accuracy) {
    // LLL-Algorithmus nach Pseudocode
    int len = matrix[0].length;
    GSO gso_M = gso(matrix, accuracy);
    BigDecimal[][] gsom = gso_M.getGsom();
    BigDecimal[][] M = gso_M.getM();

    int i = 1;
    while (i < len) {
      
      for (int j = i-1; j >= 0; j--) {
        BigDecimal zero_five = new BigDecimal("0.5");
        
        if (M[j][i].abs().compareTo(zero_five) > 0) {
          matrix[i] = vek_subtract(matrix[i], scale(matrix[j], M[j][i].setScale(0, RoundingMode.HALF_EVEN)));
          gso_M = gso(matrix, accuracy);
          gsom = gso_M.getGsom();
          M = gso_M.getM();
        }
      }

      BigDecimal zero_nine = new BigDecimal("0.9"); // Parameter delta hier setzen
      BigDecimal compare_1 = skalarprodukt(gsom[i], gsom[i]);
      BigDecimal compare_2 = (zero_nine.subtract(M[i-1][i].pow(2))).multiply(skalarprodukt(gsom[i-1], gsom[i-1]));
      
      if (compare_1.compareTo(compare_2) >= 0) {
        i += 1;
      }else {
        matrix = swap_vektors(matrix, i, i-1);
        gso_M = gso(matrix, accuracy);
        gsom = gso_M.getGsom();
        M = gso_M.getM();
        if (i > 1){
          i -= 1;
        }
      }
    }

    return matrix;
  }

}