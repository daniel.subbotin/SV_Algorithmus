# SV Algorithmus


## Ausführung

Nach dem Ausführen des Programms müssen drei Eingaben in der Kommandozeile getätigt werden. Vor jeder Eingabe
wird es eine entsprechende Nachricht geben. Es gibt insgesamt drei unterschiedliche Nachrichten.

1. "Gebe eine Subset Sum Problem Instanzgröße n an: "
-> es wird ein positiver Integer erwartet welcher die Länge der Folge a einer Subset Sum Problem Instanz darstellt

2. "Gebe H für obere Schranke X=2^H von a an: "
-> es wird wieder ein positiver Integer erwartet aus welchem die obere Schranke für die Elemente für a berechnet wird

3. "Gebe '5' (5 Versuche) oder '1' (1 Versuch) ein "
-> hier sind nur Eingaben '5' oder '1' möglich, je nachdem wie man sich entscheidet wird es nur einen Durchlauf des
SV Algorithmus auf a geben oder zusätzlich 4 weitere auf Permutationen von a, der Algorithmus terminiert sobald eine
gültige Lösung gefunden wurde


## Ungültige Eingaben

Wird eine ungültige Eingabe getroffen in Form eines negativen Integers, so wird erneut die aktuelle Nachricht
eingeblendet und man hat eine weitere Chance eine richtige Eingabe zu treffen. Eingaben welche keine Integer sind
sollten vermieden werden, da das Programm abstürzen wird.


## Achtung

Für Parameter n = 50 und H = 100 kann man mit Laufzeiten von 2 Stunden für einen Versuchsdurchlauf
rechnen. Dies ist natürlich von der Rechenleistung des genutzten Gerätes abhängig, jedoch sollte man kleinere Parameter
wählen falls man lange Laufzeiten vermeiden möchte.
